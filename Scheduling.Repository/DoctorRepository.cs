﻿using Scheduling.DB;
using Scheduling.DB.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scheduling.Repository
{
    public class DoctorRepository : IDataRepository<Doctor>
	{
		readonly ScheduleContext _dataContext;

		public DoctorRepository(ScheduleContext context)
		{
			_dataContext = context;
		}

		public void Add(Doctor doctor)
		{
			_dataContext.Doctors.Add(doctor);
			_dataContext.SaveChanges();
		}

		public void Delete(Doctor doctor)
		{
			_dataContext.Doctors.Remove(doctor);
			_dataContext.SaveChanges();
		}

		public Doctor Get(long id)
		{
			return _dataContext.Doctors
				  .FirstOrDefault(e => e.DoctorId == id);
		}

		public IEnumerable<Doctor> GetAll()
		{
			return _dataContext.Doctors.ToList();
		}

		public void Update(Doctor dbDoctor, Doctor doctor)
		{
			dbDoctor.FirstName = doctor.FirstName;
			dbDoctor.LastName = doctor.LastName;

			_dataContext.SaveChanges();
		}
	}
}

