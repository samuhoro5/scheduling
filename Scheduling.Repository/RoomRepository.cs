﻿using Scheduling.DB;
using Scheduling.DB.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scheduling.Repository
{
	public class RoomRepository : IDataRepository<Room>
	{
		readonly ScheduleContext _dataContext;

		public RoomRepository(ScheduleContext context)
		{
			_dataContext = context;
		}

		public void Add(Room room)
		{
			_dataContext.Rooms.Add(room);
			_dataContext.SaveChanges();
		}

		public void Delete(Room room)
		{
			_dataContext.Rooms.Remove(room);
			_dataContext.SaveChanges();
		}

		public Room Get(long id)
		{
			return _dataContext.Rooms
				  .FirstOrDefault(e => e.RoomId == id);
		}

		public IEnumerable<Room> GetAll()
		{
			return _dataContext.Rooms.ToList();
		}

		public void Update(Room dbRoom, Room room)
		{
			dbRoom.Name = room.Name;
			_dataContext.SaveChanges();
		}
	}
}
