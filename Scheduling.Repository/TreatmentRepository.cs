﻿using Scheduling.DB;
using Scheduling.DB.Entities;
using Scheduling.Repository.DataModels;
using Scheduling.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scheduling.Repository
{
	public class TreatmentRepository : IDataRepository<Study>, ITreatmentRepository
	{
		readonly ScheduleContext _dataContext;
		public TreatmentRepository(ScheduleContext context)
		{
			_dataContext = context;
		}

		public void Add(Study study)
		{
			_dataContext.Studies.Add(study);
			_dataContext.SaveChanges();
		}

		public void Delete(Study study)
		{
			_dataContext.Studies.Remove(study);
			_dataContext.SaveChanges();
		}

		public Study Get(long id)
		{
			
			return _dataContext.Studies
				  .FirstOrDefault(e => e.StudyId == id);
		}

		public IEnumerable<Study> GetAll()
		{						 
			return _dataContext.Studies.ToList();
		}
		
		public void Update(Study study, Study entity)
		{
			study.Description = entity.Description;
			study.StartTime = entity.StartTime;
			study.EndTime = entity.EndTime;
			study.Status = entity.Status;
			study.PatientId = entity.PatientId;
			study.DoctorId = entity.DoctorId;
			study.RoomId = entity.RoomId;

			_dataContext.SaveChanges();
		}


		public StudyDataModel GetStudyDetails(long id)
		{
			var result = (from s in _dataContext.Studies
						  join r in _dataContext.Rooms on s.RoomId equals r.RoomId
						  join p in _dataContext.Patients on s.PatientId equals p.PatientId
						  join d in _dataContext.Doctors on s.DoctorId equals d.DoctorId
						  where s.StudyId == id
						  select new StudyDataModel
						  {
							  StudyId = s.StudyId,
							  Description = s.Description,
							  StartTime = s.StartTime,
							  EndTime = s.EndTime,
							  PatientId = s.PatientId,
							  DoctorId = s.DoctorId,
							  Status = s.Status,
							  PatientName = p.FirstName + " " + p.LastName,
							  DoctorName = d.FirstName + " " + p.LastName,
							  RoomName = r.Name,
							  RoomId = r.RoomId
						  }).FirstOrDefault();
			return result;
		}
		public IEnumerable<StudyDataModel> GetStudyList()
		{
			var result = (from s in _dataContext.Studies
						  join r in _dataContext.Rooms on s.RoomId equals r.RoomId
						  join p in _dataContext.Patients on s.PatientId equals p.PatientId
						  join d in _dataContext.Doctors on s.DoctorId equals d.DoctorId
						  select new StudyDataModel
						  {
							  StudyId = s.StudyId,
							  Description = s.Description,
							  StartTime = s.StartTime,
							  EndTime = s.EndTime,
							  PatientId = s.PatientId,
							  DoctorId = s.DoctorId,
							  Status = s.Status,
							  PatientName = p.FirstName + " " + p.LastName,
							  DoctorName = d.FirstName + " " + p.LastName,
							  RoomName = r.Name,
							  RoomId = r.RoomId
						  }).ToList();
			return result;
		}
	}
}