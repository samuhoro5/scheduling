﻿using Scheduling.DB.Entities;
using Scheduling.Repository.DataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduling.Repository
{
    public interface IPatientRepository
	{
		IEnumerable<PatientDataModel> GetPatientList();
		PatientDataModel GetPatientDetails(long id);
		IEnumerable<PatientDataModel> GetUnscheduledPatientList();


	}
}
