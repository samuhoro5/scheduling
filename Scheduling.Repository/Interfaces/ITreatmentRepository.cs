﻿using Scheduling.DB.Entities;
using Scheduling.Repository.DataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduling.Repository.Interfaces
{
    public interface ITreatmentRepository
	{
		IEnumerable<StudyDataModel> GetStudyList();
		StudyDataModel GetStudyDetails(long id);
		
    }
}
