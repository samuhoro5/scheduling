﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Scheduling.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Repository
{
    public class DataRepository<T> : IDataRepository<T> where T : class
	{
		readonly ScheduleContext _dataContext;
		protected DbSet<T> DbSet;
		public DataRepository(ScheduleContext context)
		{
			_dataContext = context;
			DbSet = _dataContext.Set<T>();

		}

		public async Task<T> GetByIdAsync(object id)
		{
			return await DbSet.FindAsync(id);
		}

		public T GetById(object id)
		{
			return DbSet.Find(id);
		}
		public IEnumerable<T> GetAll()
		{
			return DbSet.AsEnumerable<T>();
		}

		public async Task<IEnumerable<T>> GetAllAsync()
		{
			return await DbSet.ToListAsync();
		}
		public T FirstOrDefault(Expression<Func<T, bool>> predicate = null)
		{
			if (predicate == null)
				return DbSet.FirstOrDefault();
			else
				return DbSet.FirstOrDefault(predicate);
		}
		public async Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate = null)
		{
			if (predicate == null)
				return await DbSet.FirstOrDefaultAsync();
			else
				return await DbSet.FirstOrDefaultAsync(predicate);
		}

		public async Task<T> FirstOrDefaultWithIncludeAsync(Expression<Func<T, bool>> predicate = null, Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null)
		{
			IQueryable<T> query = DbSet;

			if (include != null)
				query = include(query);

			if (predicate == null)
				return await query.FirstOrDefaultAsync();
			else
				return await query.FirstOrDefaultAsync(predicate);
		}
		

		public T Add(T entity)
		{
			DbSet.Add(entity);
			_dataContext.SaveChanges();
			return entity;
		}

		public T Delete(T entity)
		{
			DbSet.Remove(entity);
			_dataContext.SaveChanges();
			return entity;
		}

		public void Edit(T entity)
		{
			_dataContext.Entry(entity).State = EntityState.Modified;
			_dataContext.SaveChanges();
		}


		public async virtual Task<List<T>> FindWithIncludeAsync(
		   Expression<Func<T, bool>> filter = null,
		   Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
		   Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
										   int? start = null, int? length = null, bool disableTracking = true)
		{
			IQueryable<T> query = DbSet;

			if (disableTracking)
				query = query.AsNoTracking();

			if (filter != null)
				query = query.Where(filter);

			if (include != null)
				query = include(query);

			if (orderBy != null)
				query = orderBy(query);

			if (start.HasValue)
			{
				query = query.Skip(start.Value);
			}
			if (length.HasValue)
			{
				query = query.Take(length.Value);
			}
			return await query.ToListAsync();
		}

		public async virtual Task<List<TType>> FindWithIncludeAsync<TType>(
		   Expression<Func<T, bool>> filter = null,
		   Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
		   Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
										   int? start = null, int? length = null, bool disableTracking = true, Expression<Func<T, TType>> select = null)
		{
			IQueryable<T> query = DbSet;

			if (disableTracking)
				query = query.AsNoTracking();

			if (filter != null)
				query = query.Where(filter);

			if (include != null)
				query = include(query);

			if (orderBy != null)
				query = orderBy(query);

			if (start.HasValue)
			{
				query = query.Skip(start.Value);
			}
			if (length.HasValue)
			{
				return await query.Take(length.Value).Select(select).ToListAsync();
			}
			else
			{
				return await query.Select(select).ToListAsync();
			}

		}
	}
}
