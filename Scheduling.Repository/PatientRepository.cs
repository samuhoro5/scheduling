﻿using Scheduling.DB;
using Scheduling.DB.Entities;
using Scheduling.Repository.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scheduling.Repository
{
	public class PatientRepository : IDataRepository<Patient>, IPatientRepository
	{
		readonly ScheduleContext _dataContext;

		public PatientRepository(ScheduleContext context)
		{
			_dataContext = context;
		}

		public void Add(Patient patient)
		{
			_dataContext.Patients.Add(patient);
			_dataContext.SaveChanges();
		}

		public void Delete(Patient patient)
		{
			_dataContext.Patients.Remove(patient);
			_dataContext.SaveChanges();
		}

		public Patient Get(long id)
		{
			return _dataContext.Patients
				  .FirstOrDefault(e => e.PatientId == id);
		}

		public IEnumerable<Patient> GetAll()
		{		
			
			var patientIdList = (from p in _dataContext.Studies
							 select p.PatientId)
						  .ToList();

			var result = (from p in _dataContext.Patients
							   where !patientIdList.Contains(p.PatientId)
							   select p)
						  .ToList();

			return result;
		}

		public PatientDataModel GetPatientDetails(long id)
		{
			var result = (from p in _dataContext.Patients
						  where p.PatientId == id
						  select new PatientDataModel
						  {
							  FirstName = p.FirstName,
							  LastName = p.LastName,
							  DateOfBirth = p.DateOfBirth,
							  Gender = p.Gender,
							  PatientId = p.PatientId
						  }).FirstOrDefault();
			return result;
		}


		public IEnumerable<PatientDataModel> GetPatientList()
		{
			var result = (from p in _dataContext.Patients
						  select new PatientDataModel
						  {
							 FirstName = p.FirstName,
							 LastName = p.LastName,
							 DateOfBirth =p.DateOfBirth,
							 Gender = p.Gender,
							 PatientId =p.PatientId
						  }).ToList();
			return result;
		}

		public IEnumerable<PatientDataModel> GetUnscheduledPatientList()
		{
			var patientIdList = (from p in _dataContext.Studies
								 select p.PatientId)
						  .ToList();

			var result = (from p in _dataContext.Patients
						  where !patientIdList.Contains(p.PatientId)
						  select new PatientDataModel
						  {
							  FirstName = p.FirstName,
							  LastName = p.LastName,
							  DateOfBirth = p.DateOfBirth,
							  Gender = p.Gender,
							  PatientId = p.PatientId,
						  }).ToList();

			return result;
		}

		public void Update(Patient dbPatient, Patient patient)
		{
			dbPatient.FirstName = patient.FirstName;
			dbPatient.LastName = patient.LastName;
			dbPatient.Gender = patient.Gender;
			dbPatient.DateOfBirth = patient.DateOfBirth;
			_dataContext.SaveChanges();
		}
	}
}
