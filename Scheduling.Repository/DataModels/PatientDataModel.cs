﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduling.Repository.DataModels
{
    public class PatientDataModel
    {
		public long PatientId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Gender { get; set; }
		public DateTime? DateOfBirth { get; set; }
	}
}
