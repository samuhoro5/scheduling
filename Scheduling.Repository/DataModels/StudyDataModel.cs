﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduling.Repository.DataModels
{
   public class StudyDataModel
    {

		public long StudyId { get; set; }
		public string Description { get; set; }
		public string Status { get; set; }
		public DateTime StartTime { get; set; }
		public DateTime? EndTime { get; set; }
		public long PatientId { get; set; }
		public long DoctorId { get; set; }
		public long RoomId { get; set; }
		public string PatientName { get; set; }
		public string RoomName { get; set; }
		public string DoctorName { get; set; }
	}
}
