using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Scheduling.DB;
using Scheduling.DB.Entities;
using Scheduling.Repository;
using Scheduling.Repository.Interfaces;
using System.Net;

namespace Scheduling
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });

			services.AddDbContext<ScheduleContext>(opts => opts.UseSqlServer(Configuration["ConnectionStrings:SchedulingDatabase"]));
			services.AddScoped<IDataRepository<Doctor>, DoctorRepository>();
			services.AddScoped<IDataRepository<Room>, RoomRepository>();
			services.AddScoped<IDataRepository<Patient>, PatientRepository>();
			services.AddScoped<IDataRepository<Study>, TreatmentRepository>();
			services.AddScoped<IPatientRepository, PatientRepository>();
			services.AddScoped<ITreatmentRepository, TreatmentRepository>();
			services.AddCors();
			services.Configure<ApiBehaviorOptions>(apiBehaviorOptions =>
			{
				apiBehaviorOptions.InvalidModelStateResponseFactory =
						actionContext =>
						{
							return new BadRequestObjectResult(actionContext.ModelState)
							{
								StatusCode = (int?)HttpStatusCode.UnprocessableEntity
							};
						};
			});
			services.AddAutoMapper();
		}

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }
    }
}
