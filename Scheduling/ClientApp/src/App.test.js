import React from 'react';
import shallow from 'enzyme';
import App from './App';
import RoutePage from './navigation/RoutePage';

// test('Check route page componet on App', () => {
//   const wrapper = shallow(<App />);
//   expect(wrapper.find('RoutePage').length).toEqual(1);
// });


describe('App', () => {
  let wrapper;

  beforeEach(() => wrapper = shallow(<App />));

  it('should render the RoutePage Component', () => {
    expect(wrapper.containsMatchingElement(<RoutePage />)).toEqual(true);
  });

  it('should render a <div />', () => {
    expect(wrapper.find('div').length).toEqual(1);
  });
});