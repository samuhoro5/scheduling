import { applyMiddleware, combineReducers, createStore } from 'redux';
import PatientReducer from './Reducers/PatientReducer';
import RoomReducer from './Reducers/RoomReducer';
import DoctorReducer from './Reducers/DoctorReducer';
import StudyReducer from './Reducers/StudyReducer';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

// store for putting state 
export const storeConfig = () => {
    const appReducer = combineReducers({
        patientReducer: PatientReducer,
        roomReducer: RoomReducer,
        doctorReducer: DoctorReducer,
        studyReducer: StudyReducer
    });

    const rootReducer = (state, action) => {
        return appReducer(state, action)
    }

    const store = createStore(
        rootReducer,
        composeWithDevTools(applyMiddleware(thunk))
    )

    return store;
}