import { getDoctorsAPI } from "../../services/doctorService";

// actions methods for dispatch action and updated state
export const getDoctors = () => (
    async (dispatch) => {
        try {
            let response = await getDoctorsAPI();
            if (response.status === 200) {
                let doctors = response.data
                dispatch({
                    type: 'SET_DOCTORS',
                    doctors
                });
                return true;
            }
        } catch (error) {
            console.log(error.response);
        }
        return false;


    }
);
