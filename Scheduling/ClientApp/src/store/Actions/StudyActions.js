import { addStudyAPI, getStudyAPI, getStudiesAPI, updateStudyAPI } from '../../services/studyService';

// actions methods for dispatch action and updated state
export const addStudy = (study) => async (dispatch) => {
    try {
        let response = await addStudyAPI(study);
        if (response.status === 200) {
            let study = response.data;
            dispatch({
                type: 'SET_STUDY',
                study
            });
            return true;
        }
    } catch (error) {
        console.log(error.response);
    }
    return false;
}

export const getStudies = () => async (dispatch) => {
    try {
        let response = await getStudiesAPI();
        if (response.status === 200) {
            let newStudyList = response.data;
            dispatch({
                type: 'SET_STUDIES',
                newStudyList
            })
            return true;
        }
    } catch (error) {
        console.log(error.response);
    } return false;
}

export const getStudy = (id) => async (dispatch) => {
    try {
        let response = await getStudyAPI(id);
        if (response.status === 200) {
            let study = response.data;
            dispatch({
                type: 'SET_STUDY',
                study
            });
            return true;
        }
    } catch (error) {
        console.log(error.response);
    }
    return false;
}

export const updateStatus = (status) => async (dispatch) => {
    dispatch({
        type: 'UPDATE_STATUS',
        status
    })
}

export const updateStudy = (study) => async (dispatch) => {
    try {
        let response = await updateStudyAPI(study);
        if (response.status === 200) {
            dispatch({
                type: 'UPDATE_STUDIES',
                study
            });
            dispatch({
                type: 'SET_STUDY',
                study: ""
            });
            return true;
        }
    } catch (error) {
        console.log(error.response, error);
    }
    return false;
}

export const updateStudies = (studies) => async (dispatch) => {
    dispatch({
        type: 'SET_STUDIES',
        studies
    })
}

export const resetStudy = () => async (dispatch) => {
    let study = "";
    dispatch({
        type: 'SET_STUDY',
        study
    })
}
