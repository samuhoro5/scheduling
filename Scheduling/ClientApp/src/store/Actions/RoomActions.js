import { getRoomsAPI } from '../../services/roomService';

// actions methods for dispatch action and updated state
export const getRooms = () => (
    async (dispatch) => {
        // const url = `api/room`;
        // const response = await fetch(url);
        // const rooms = await response.json();
        try {

            let response = await getRoomsAPI();
            if (response.status === 200) {
                let rooms = response.data;

                dispatch({
                    type: 'SET_ROOMS',
                    rooms
                });
                return true;
            }
        } catch (error) {
            console.log(error.response);
        }
        return false;
    }
);
