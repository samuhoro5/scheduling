import { addPatientAPI, getPatientAPI, getPatientListAPI } from '../../services/patientService';

// actions methods for dispatch action and updated state
export const addPatient = (patient) => (
    async (dispatch) => {
        try {
            let response = await addPatientAPI(patient);
            if (response.status === 200) {
                let patient = response.data;
                dispatch({
                    type: 'SET_PATIENT',
                    patient
                });
                return true;
            }
        } catch (error) {
            console.log(error.response);
        }
        return false;
    }
);

export const getPatients = () => async (dispatch) => {
    try {
        let response = await getPatientListAPI();
        if (response.status === 200) {
            let patients = response.data;
            dispatch({
                type: 'SET_PATIENTS',
                patients
            });
            return true;
        }
    } catch (error) {
        console.log(error.response);
    }
    return false;
}

export const getPatient = (id) => async (dispatch) => {
    try {
        let response = await getPatientAPI(id);
        if (response.status === 200) {
            let patient = response.data;
            dispatch({
                type: 'SET_PATIENT',
                patient
            })
            return true;
        }
    } catch (error) {
        console.log(error.response);
    }
    return false;
}

export const resetPatient = () => async (dispatch) => {
    dispatch({
        type: 'RESET_PATIENT'
    })
}

