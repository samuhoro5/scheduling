
const PATIENT = {
    firstName: '',
    lastName: '',
    gender: 'male',
    dateOfBirth: '',
    roomId: '',
}

// define initial state variables
const initState = {
    patients: null,
    patient: { PATIENT },
    errorMessage: ""
}

// define reducer for state update on the basis of action type.
const PatientReducer = (state = initState, action) => {

    switch (action.type) {
        case 'SET_PATIENTS':
            return ({
                ...state,
                patients: action.patients
            })
        case 'SET_PATIENT':
            return ({
                ...state,
                patient: action.patient
            })

        case 'SET_ERROR':
            return ({
                ...state,
                errorMessage: action.errorMessage
            })

        case 'RESET_PATIENT':
            return ({
                ...state,
                patient: { PATIENT }
            })

        case 'RESET_ERROR':
            return ({
                ...state,
                errorMessage: ""
            })
        default:
            return state;
    }

}

export default PatientReducer;