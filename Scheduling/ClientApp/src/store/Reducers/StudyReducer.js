
// define initial state variables
const initState = {
    studies: [],
    study: null
}

// define reducer for state update on the basis of action type.
const StudyReducer = (state = initState, action) => {

    switch (action.type) {
        case 'SET_STUDIES':
            return ({
                ...state,
                studies: action.newStudyList
            });

        case 'SET_STUDY':
            return ({
                ...state,
                study: action.study
            });

        case 'UPDATE_STUDIES':
            let newStudyList = state.studies.map(s => {
                if (s.studyId === action.study.studyId) {
                    return action.study;
                }
                return s;
            })
            return ({
                ...state,
                studies: newStudyList
            });

        case 'UPDATE_STATUS':
            return ({
                ...state,
                study: { ...state.study, status: action.status }
            });

        default:
            return state;
    }

}


export default StudyReducer;