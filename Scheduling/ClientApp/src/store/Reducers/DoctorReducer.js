// define initial state variables
const initState = {
    doctors: ""
}

// define reducer for state update on the basis of action type.
const DoctorReducer = (state = initState, action) => {

    switch (action.type) {
        case 'SET_DOCTORS':
            return ({
                ...state,
                doctors: action.doctors
            })
        default:
            return state;
    }

}

export default DoctorReducer;