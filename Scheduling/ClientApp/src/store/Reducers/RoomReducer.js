

// define initial state variables
const initState = {
    rooms: null

}

// define reducer for state update on the basis of action type.
const RoomReducer = (state = initState, action) => {
    switch (action.type) {
        case 'SET_ROOMS':
            return ({
                ...state,
                rooms: action.rooms
            });

        default:
            return state;
    }

}


export default RoomReducer;