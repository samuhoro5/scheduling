import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Patient from '../containers/Patient';
import Schedule from '../containers/Schedule';
import UpdateStatus from '../containers/UpdateStatus';
import Layout from '../components/Layout';

const RoutePage = () => {
    return (
        <Layout>
            <Switch>
                <Route exact path='/' component={Patient} />
                <Route path='/schedule' component={Schedule} />
                <Route path='/updateStatus' component={UpdateStatus} />
            </Switch>
        </Layout>
    );
}

export default RoutePage;