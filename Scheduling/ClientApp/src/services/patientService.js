import apiInstance from './axiosConfig';

export const addPatientAPI = async (patient) => {
    return await apiInstance.post('/patient/', patient);
}

export const getPatientListAPI = async () => {
    return await apiInstance.get('/patient/');
}

export const getPatientAPI = async (id) => {
    return await apiInstance.get('/patient/' + id);
}