import apiInstance from './axiosConfig';

export const addStudyAPI = async (study) => {
    return await apiInstance.post('/treatment/', study);
}

export const updateStudyAPI = async (study) => {
    return await apiInstance.put('/treatment/' + study.studyId, study);
}


export const getStudiesAPI = async () => {
    return await apiInstance.get('/treatment/');
}

export const getStudyAPI = async (id) => {
    return await apiInstance.get('/treatment/' + id);
}