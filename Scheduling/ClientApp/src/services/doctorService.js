import apiInstance from './axiosConfig';

export const getDoctorsAPI = async () => {
    return await apiInstance.get('/doctor');
}