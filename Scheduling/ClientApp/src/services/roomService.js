import apiInstance from './axiosConfig';

export const getRoomsAPI = async () => {
    return await apiInstance.get('/room');
}