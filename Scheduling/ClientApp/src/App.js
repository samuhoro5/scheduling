﻿import React from 'react';
import './App.css';
import { BrowserRouter } from "react-router-dom";
import RoutePage from './navigation/RoutePage';
import { Provider } from 'react-redux';
import { storeConfig } from './store/storeConfig';


const store = storeConfig();


function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <RoutePage />
        <div />
      </BrowserRouter>
    </Provider>
  );
}

export default App;
