import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import toastr from 'toastr';
import 'toastr/build/toastr.min.css';

import * as patientActions from '../store/Actions/PatientActions';
import * as studyActions from '../store/Actions/StudyActions';
import * as doctorActions from '../store/Actions/DoctorActions';
import * as roomActions from '../store/Actions/RoomActions';
import PatientCard from '../components/PatientCard';
import ScheduleCard from '../components/ScheduleCard';

const STUDY = {
    description: "",
    status: "",
    startTime: "",
    endTime: "",
    patientId: "",
    doctorId: "",
    roomId: ""
}

const ERROR = {
    description: "",
    status: "",
    startTime: "",
    endTime: "",
    doctorId: "",
    roomId: ""
}

class Schedule extends Component {

    state = {
        patientId: "",
        study: { ...STUDY },
        errorMsg: { ...ERROR }
    }
    componentDidMount() {
        this.getPatientList();
        this.props.getDoctors();
        this.props.getRooms();
    }

    // Get the list of unscheduled patient
    getPatientList = async () => {
        var result = await this.props.getPatients();
        if (!result) {
            toastr.error("Oops, something went wrong!");
        }
    }

    // Get the value on select change event
    selectHandler = (event) => {
        this.resetForm();
        const patientId = event.target.value;
        this.setState({ study: { ...this.state.study, patientId } });
        this.props.getPatient(patientId);

    }

    // Get the value on change event
    changeHandler = (event) => {
        this.setState({
            study: {
                ...this.state.study,
                [event.target.id]: event.target.value
            }
        }, () => this.validation());
    }

    // reset the study 
    resetForm = () => {
        this.setState({ ...this.state, study: STUDY })
    }

    // Validate the forms and check the required fields.
    validation = () => {
        let { study } = this.state;
        let errorMsg = { ...this.state.errorMsg };
        let flag = true;
        let today = new Date();
        if (study.doctorId.trim() === "") {
            errorMsg.doctorId = "Please setect doctor.";
            flag = false;
        }
        else {
            errorMsg.doctorId = "";
        }
        if (study.description.trim() === "") {
            errorMsg.description = "Please enter description";
            flag = false;
        }
        else {
            errorMsg.description = "";
        }

        if (study.startTime.trim() === "") {
            errorMsg.startTime = "Please enter start time";
            flag = false;
        }
        else if (moment(study.startTime).format("YYYY-MM-DD HH:MM") < moment(today).format("YYYY-MM-DD HH:MM")) {
            errorMsg.startTime = "Start time should be today or future date time";
            flag = false;
        }
        else {
            errorMsg.startTime = "";
        }

        if (study.endTime.trim() !== "" && moment(study.startTime).format("YYYY-MM-DD HH:MM") > moment(study.endTime).format("YYYY-MM-DD HH:MM")) {
            errorMsg.endTime = "End time should be greater than Start Time";
            flag = false;
        }
        else {
            errorMsg.endTime = "";
        }

        if (study.status.trim() === "") {
            errorMsg.status = "Please select status"
            flag = false;
        }
        else {
            errorMsg.status = "";
        }

        if (study.roomId.trim() === "") {
            errorMsg.roomId = "Please select room"
            flag = false;
        }
        else {
            errorMsg.roomId = "";
        }

        this.setState({
            errorMsg
        })


        return flag;
    }

    // Submit the form and create new procedure schedule
    submitHandler = async () => {
        if (this.validation()) {
            let { study } = this.state;
            study.patientId = this.props.patient.patientId;
            const result = await this.props.addStudy(study);
            if (result) {
                this.resetForm();
                toastr.success("Procedure Scheduled!");
                this.getPatientList();
            } else {
                toastr.error("Oops, something went wrong!");
            }
        }
    }

    // Reset the patient and study data
    restStudyHandler = () => {
        this.props.resetStudy();
        this.props.resetPatient();
    }

    // JSX for UI
    render() {
        return (
            <>
                {this.props.study ?
                    <ScheduleCard patient={this.props.patient} study={this.props.study} header="Procedure Scheduled!!" resetStudy={this.restStudyHandler} /> :
                    <div>
                        <h4 className="text-center mt-3">Schedule</h4>
                        <label>Select Patient<span className="text-danger">*</span></label>
                        <div className="row">
                            <div className="col-sm-6">
                                <select className="form-control form-control-sm"
                                    onChange={this.selectHandler}
                                    value={this.props.patient.patientId}>
                                    <option>Choose...</option>
                                    {this.props.patients && this.props.patients.map(patient =>
                                        <option key={patient.patientId} value={patient.patientId}>{patient.firstName + ' ' + patient.lastName}</option>)}
                                </select>
                            </div>
                        </div>

                        {this.props.patient.patientId &&
                            <>
                                <PatientCard patient={this.props.patient} resetForm={this.resetFormHandler} type='2' header="Details" />
                                <div className="card mt-3 mb-3">
                                    <div className="card-body">
                                        <h5 className="card-title text-center text-primary">Add Schedule</h5>
                                        <div className="row">
                                            <div className="col-sm">
                                                <div className="form-group">
                                                    <label>Doctor<span className="text-danger">*</span></label>
                                                    <select className="form-control form-control-sm" id="doctorId"
                                                        onChange={this.changeHandler}
                                                        value={this.state.study.doctorId}>
                                                        <option>Choose...</option>
                                                        {this.props.doctors && this.props.doctors.map(doctor =>
                                                            <option key={doctor.doctorId} value={doctor.doctorId}>{doctor.firstName + ' ' + doctor.lastName}</option>)}
                                                    </select>
                                                    <small className="text-danger">{this.state.errorMsg.doctorId}</small>
                                                </div>
                                            </div>
                                            <div className="col-sm">
                                                <div className="form-group">
                                                    <label>Room<span className="text-danger">*</span></label>
                                                    <select className="form-control form-control-sm" id="roomId"
                                                        onChange={this.changeHandler}
                                                        value={this.state.study.roomId}
                                                    >
                                                        <option>Choose...</option>
                                                        {this.props.rooms && this.props.rooms.map(room =>
                                                            <option key={room.roomId} value={room.roomId}>{room.name}</option>)}
                                                    </select>
                                                    <small className="text-danger">{this.state.errorMsg.roomId}</small>
                                                </div>
                                            </div>
                                        </div>

                                        <form>
                                            <div className="row">
                                                <div className="col-sm">
                                                    <div className="form-group">
                                                        <label>Description <span className="text-danger">*</span></label>
                                                        <textarea rows="5" type="textarea" className="form-control form-control-sm" placeholder="Description" id="description"
                                                            value={this.state.study.description}
                                                            onChange={this.changeHandler}
                                                        />
                                                        <small className="text-danger">{this.state.errorMsg.description}</small>
                                                    </div>
                                                </div>
                                                <div className="col-sm">
                                                    <div className="form-group">
                                                        <label>Status<span className="text-danger">*</span></label>
                                                        <select className="form-control form-control-sm" id="status"
                                                            onChange={this.changeHandler}
                                                            value={this.state.study.status}
                                                        >
                                                            <option>Choose...</option>
                                                            <option key={1} value="Planned">Planned</option>
                                                        </select>
                                                        <small className="text-danger">{this.state.errorMsg.status}</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm">
                                                    <div className="form-group">
                                                        <label>Planned Start Time <span className="text-danger">*</span></label>
                                                        <input type="datetime-local" className="form-control form-control-sm" id="startTime"
                                                            value={this.state.study.startTime}
                                                            onChange={this.changeHandler}
                                                        />
                                                        <small className="text-danger">{this.state.errorMsg.startTime}</small>
                                                    </div>
                                                </div>
                                                <div className="col-sm">
                                                    <div className="form-group">
                                                        <label>Estimated End Time</label>
                                                        <input type="datetime-local" className="form-control form-control-sm" id="endTime"
                                                            value={this.state.study.endTime}
                                                            onChange={this.changeHandler}
                                                        />
                                                        <small className="text-danger">{this.state.errorMsg.endTime}</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">

                                            </div>
                                            <button type="button" className="btn btn-primary btn-sm" onClick={this.submitHandler}><span className="fa fa-send-o mr-1"></span>Submit</button>
                                        </form>
                                    </div>
                                </div>
                            </>}
                    </div>
                }
            </>
        );
    }
}

// Get all the requied state values from reducer
const mapStateToProps = state => {
    return {
        study: state.studyReducer.study,
        patients: state.patientReducer.patients,
        patient: state.patientReducer.patient,
        doctors: state.doctorReducer.doctors,
        rooms: state.roomReducer.rooms
    }
}

// Get all the actions method from action creators
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        ...studyActions,
        ...patientActions,
        ...doctorActions,
        ...roomActions
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Schedule);
