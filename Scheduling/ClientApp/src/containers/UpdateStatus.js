import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import toastr from 'toastr';
import 'toastr/build/toastr.min.css';
import moment from 'moment';

import * as patientActions from '../store/Actions/PatientActions';
import * as studyActions from '../store/Actions/StudyActions';
import ScheduleCard from '../components/ScheduleCard';


class UpdateStatus extends Component {

    componentDidMount() {
        this.props.getStudies();
    }

    // Open the edit UI for update status
    editHandler = (patientId, studyId) => {
        this.props.getPatient(patientId);
        this.props.getStudy(studyId);
    }

    // Get the value of status change
    changeHandler = (event) => {
        this.props.updateStatus(event.target.value);
    }

    //Update the status of procedure
    updateHandler = async () => {
        let study = this.props.study;
        const result = await this.props.updateStudy(study);
        if (result) {
            this.props.resetPatient();
            toastr.success('Status Updated!');
        } else {
            toastr.error("Oops, something went wrong!");
        }
    }

    // Get the data for list view
    listHandler = () => {
        this.props.resetPatient();
        this.props.resetStudy();
    }

    //JSX code for UI
    render() {
        return (
            <div>
                {this.props.study ?
                    <>
                        <ScheduleCard patient={this.props.patient} study={this.props.study} header="Update Status" />
                        <div className="card mt-1">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <label>Status<span className="text-danger">*</span></label>
                                            <select className="form-control form-control-sm" id="status"
                                                onChange={this.changeHandler}
                                                value={this.props.study.status}
                                            >
                                                <option key={1} value="Planned">Planned</option>
                                                <option key={2} value="InProgress">InProgress</option>
                                                <option key={3} value="Finished">Finished</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <button type="button" className="btn btn-sm btn-primary mr-2" onClick={this.updateHandler}><span className="fa fa-refresh mr-1"></span>Update</button>
                                <button type="button" className="btn btn-sm btn-primary mr-2" onClick={this.listHandler}><span className="fa fa-list mr-1"></span>Go To List</button>
                            </div>
                        </div>
                    </> :
                    <>
                        <h4 className="text-center mt-3">Update Status</h4>
                        {this.props.studies.length > 0 ? <table className="table table-striped table-sm">
                            <thead>
                                <tr>
                                    <th scope="col">Patient Name</th>
                                    <th scope="col">Doctor Name</th>
                                    <th scope="col">Room</th>
                                    <th scope="col">Start Time</th>
                                    <th scope="col">Status</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.props.studies.map(study =>
                                    <tr key={study.studyId}>
                                        <td>{study.patientName}</td>
                                        <td>{study.doctorName}</td>
                                        <td>{study.roomName}</td>
                                        <td>{moment(study.startTime).format("DD-MMM-YYYY, hh:mm a")}</td>
                                        <td>{study.status}</td>
                                        <td><span className="fa fa-edit mr-1 text-primary" onClick={() => this.editHandler(study.patientId, study.studyId)} /></td>
                                    </tr>)
                                }
                            </tbody>
                        </table>
                            :
                            <p className="text-center text-primary mt-5">No Procedure Scheduled!!</p>
                        }
                    </>
                }
            </div>
        );
    }
}

// Get all the requied state values from reducer
const mapStateToProps = state => {
    return {
        study: state.studyReducer.study,
        patients: state.patientReducer.patients,
        patient: state.patientReducer.patient,
        studies: state.studyReducer.studies
    }
}

// Get all the actions method from action creators
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        ...studyActions,
        ...patientActions
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdateStatus);
