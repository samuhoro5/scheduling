import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import toastr from 'toastr';
import 'toastr/build/toastr.min.css';

import * as roomActions from '../store/Actions/RoomActions';
import * as patientActions from '../store/Actions/PatientActions';
import PatientCard from '../components/PatientCard';



const PATIENT = {
    firstName: '',
    lastName: '',
    gender: '',
    dateOfBirth: '',
    roomId: '',
}

class Patient extends Component {

    state = {
        patient: { ...PATIENT },
        firstNameError: '',
        lastNameError: '',
        dobError: '',
        roomError: ''
    }

    componentDidMount() {
        this.props.resetPatient();

    }

    // Get the value of controlsand update state while it changing
    changeHandler = (event) => {
        this.setState({
            patient: {
                ...this.state.patient,
                [event.target.id]: event.target.value
            }
        })

        if (this.state.firstNameError) {
            this.setState({
                firstNameError: ''
            })
        }

        if (this.state.lastNameError) {
            this.setState({
                lastNameError: ''
            })
        }

        if (this.state.dobError) {
            this.setState({
                dobError: ''
            })
        }

        if (this.state.roomError) {
            this.setState({
                roomError: ''
            })
        }
    }

    // Get the value of gender 
    checkHandler = (event) => {
        this.setState({
            patient: {
                ...this.state.patient,
                [event.target.name]: event.target.value
            }
        })

    }

    // Submit the form and add new user
    submitHandler = async () => {
        if (this.validation()) {
            let { patient } = this.state;
            const result = await this.props.addPatient(patient);
            if (result) {
                this.setState({ ...this.state, patient: PATIENT });
                toastr.success("Patient added!");
            } else {
                toastr.error("Oops, something went wrong!");
            }
        }
    }

    // Validate the forms and check the required fields.
    validation = () => {
        let { patient } = this.state;
        let flag = true;
        let today = new Date();
        if (patient.firstName.trim() === "") {
            this.setState({
                firstNameError: "Please enter first name."
            })
            flag = false;
        }
        if (patient.lastName.trim() === "") {
            this.setState({
                lastNameError: "Please enter last name."
            })
            flag = false;
        }
        if (patient.dateOfBirth > moment(today).format("YYYY-MM-DD")) {
            this.setState({
                dobError: "DOB should be today or past date."
            })
            flag = false;
        }

        return flag;
    }

    // reset the patient data 
    resetFormHandler = () => {
        this.props.resetPatient();
    }

    // JSX code for UI
    render() {
        return (
            <>
                {this.props.patient.patientId ?
                    <PatientCard patient={this.props.patient} resetForm={this.resetFormHandler} type='1' header="Added" /> :
                    <div className="card mt-3 mb-3">
                        <div className="card-body">
                            <h5 className="card-title text-center text-primary">Add Patient</h5>
                            <form>
                                <div className="row">
                                    <div className="col-sm">
                                        <div className="form-group">
                                            <label>First Name <span className="text-danger">*</span></label>
                                            <input type="text" className="form-control form-control-sm" placeholder="First Name" id="firstName"
                                                value={this.state.patient.firstName}
                                                onChange={this.changeHandler}
                                            />
                                            <small className="text-danger">{this.state.firstNameError}</small>
                                        </div>
                                    </div>
                                    <div className="col-sm">
                                        <div className="form-group">
                                            <label>Last Name <span className="text-danger">*</span></label>
                                            <input type="text" className="form-control form-control-sm" placeholder="Last Name" id="lastName"
                                                value={this.state.patient.lastName}
                                                onChange={this.changeHandler}
                                            />
                                            <small className="text-danger">{this.state.lastNameError}</small>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-sm">
                                        <div className="form-group">
                                            <label>Date of birth</label>
                                            <input type="date" className="form-control form-control-sm" placeholder="Date of birth" id="dateOfBirth"
                                                value={this.state.patient.dateOfBirth}
                                                onChange={this.changeHandler}
                                            />
                                            <small className="text-danger">{this.state.dobError}</small>
                                        </div>
                                    </div>
                                    <div className="col-sm">
                                        <div className="form-group">
                                            <label>Gender</label><br />
                                            <div className="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="genderRadioMale" name="gender" className="custom-control-input"
                                                    value="male" onChange={this.checkHandler}
                                                    checked={"male" === this.state.patient.gender}
                                                />
                                                <label className="custom-control-label" htmlFor="genderRadioMale">Male</label>
                                            </div>
                                            <div className="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="genderRadioFemale" name="gender" className="custom-control-input"
                                                    value="female" onChange={this.checkHandler}
                                                    checked={"female" === this.state.patient.gender}
                                                />
                                                <label className="custom-control-label" htmlFor="genderRadioFemale">Female</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <button type="button" className="btn btn-primary btn-sm" onClick={this.submitHandler}><span className="fa fa-send-o mr-1"></span>Submit</button>
                            </form>
                        </div></div>
                }
            </>
        );
    }
}

// Get all the requied state values from reducer
const mapStateToProps = state => {
    return {
        rooms: state.roomReducer.rooms,
        patient: state.patientReducer.patient
    }
}

// Get all the actions method from action creators
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        ...patientActions
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Patient);
