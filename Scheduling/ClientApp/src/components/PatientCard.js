import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';

const PatientCard = (props) => {
    const { patient, resetForm, header, type } = props;
    const resetFormHandler = () => {
        resetForm();
    }

    return (
        <div className="card mt-3">
            <div className="card-body">
                <h5 className="card-title text-center">{"Patient " + header}</h5>
                <div className="row">
                    <div className="col-sm">
                        <p className="card-text">ID: {patient.patientId}</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <p className="card-text">Name: {patient.firstName + ' ' + patient.lastName}</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <p className="card-text">Date Of Birth: {patient.dateOfBirth && moment(patient.dateOfBirth).format("DD-MMM-YYYY")}</p>
                    </div>
                    <div className="col-sm">
                        <p className="card-text">Gender: {patient.gender}</p>
                    </div>
                </div>

                {type === "1" &&
                    <>
                        <Link to="/" className="btn btn-sm btn-primary mt-2 mr-2" onClick={resetFormHandler}><span className="fa fa-plus mr-1"></span>Add Patient</Link>

                        <Link to="/schedule" className="btn btn-sm btn-primary mt-2"><span className="fa fa-calendar mr-1"></span>Schedule</Link>

                    </>}
            </div>
        </div>
    )
}

export default PatientCard;