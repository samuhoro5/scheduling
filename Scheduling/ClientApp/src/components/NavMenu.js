import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import './NavMenu.css';

export default props => {

    // Toggle the nav bar for small screen
    const toggle = () => {
        let e = document.getElementById("navbarSupportedContent");
        if (e.className.includes(" show")) {
            e.className = e.className.replace(" show", "");
        } else {
            e.className += " show";
        }
    }

    // JSX for navigation
    return (
        <nav className="navbar navbar-expand-md navbar-inverse bg-dark fixed-top">
            <div className="navbar-header">
                <Link className="" to="/">Treatment</Link>
                <button className="navbar-toggler float-right" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation" onClick={toggle}>
                    <span className="fa fa-bars"></span>
                </button>
            </div>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="nav navbar-nav">
                    <li className="nav-item">
                        <NavLink exact to="/" onClick={toggle}>
                            <span className="fa fa-plus pr-2"></span>
                            Add Patient
                        </NavLink>
                    </li>
                    <li className="nav-item" onClick={toggle}>
                        <NavLink className="" to="/schedule">
                            <span className="fa fa-calendar pr-2"></span>
                            Schedule
                        </NavLink>
                    </li>
                    <li className="nav-item" onClick={toggle}>
                        <NavLink className="" to="/updateStatus">
                            <span className="fa fa-list pr-2"></span>
                            Scheduled List
                        </NavLink>
                    </li>
                </ul>
            </div>
        </nav>
    );
}
