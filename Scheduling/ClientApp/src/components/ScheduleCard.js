import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';

const ScheduleCard = (props) => {
    const { study, patient, header, resetStudy } = props;

    const resetStudyHandler = () => {
        resetStudy();
    }
    return (
        <div className="card mt-3">
            <div className="card-body">
                <h5 className="card-title text-center">{header}</h5>
                <div className="row">
                    <div className="col-sm">
                        <p className="card-text">Patient ID: {patient.patientId}</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <p className="card-text">Name: {patient.firstName + ' ' + patient.lastName}</p>
                    </div>
                    <div className="col-sm">
                        <p className="card-text">Gender: {patient.gender}</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <p className="card-text">Date Of Birth: {patient.dateOfBirth && moment(patient.dateOfBirth).format("DD-MMMM-YYYY")}</p>
                    </div>
                    <div className="col-sm">
                        <p className="card-text">Alotted Room: {study.roomName}</p>
                    </div>
                </div>

                <div className="row">
                    <div className="col-sm">
                        <p className="card-text">Doctor: {study.doctorName}</p>
                    </div>
                    <div className="col-sm">
                        <p className="card-text">Status: {study.status}</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <p className="card-text">Estimated End Time: {study.endTime && moment(study.endTime).format("DD-MMM-YYYY, hh:mm a")}</p>

                    </div>

                    <div className="col-sm">
                        <p className="card-text">Planned Start Time: {moment(study.startTime).format("DD-MMM-YYYY, hh:mm a")}</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <p className="card-text">Description: {study.description}</p>
                    </div>
                </div>
                {header !== "Update Status" &&
                    <>
                        <button type="button" className="btn btn-sm btn-primary mt-2 mr-2" onClick={resetStudyHandler}><span className="fa fa-calendar mr-2"></span>Schedule More</button>
                        <Link to="/updateStatus" className="btn btn-sm btn-primary mt-2 mr-2"><span className="fa fa-edit mr-2"></span>Update Status</Link>
                    </>}
            </div>
        </div>
    )
}

export default ScheduleCard;