import React from 'react';
import NavMenu from './NavMenu';

export default props => (
    <div className="container-fluid">
        <div className="row">
            <div className="col-md-3 side-bar" style={{ color: "gray" }}>
                <NavMenu />
            </div>
            <div className="col-md-9">
                {props.children}
            </div>
        </div>
    </div >
);