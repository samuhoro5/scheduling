﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Scheduling.DB.Entities;
using Scheduling.Models;
using Scheduling.Repository;

namespace Scheduling.Controllers
{
	[Route("api/doctor")]
	public class DoctorController : Controller
	{
		private readonly IDataRepository<Doctor> _dataRepository;
		private readonly IMapper _mapper;

		public DoctorController(IMapper mapper, IDataRepository<Doctor> dataRepository)
		{
			_dataRepository = dataRepository;
			_mapper = mapper;
		}

		// GET: api/Doctor/1
		[HttpGet]
		public IActionResult Get()
		{
			IEnumerable<Doctor> doctors = _dataRepository.GetAll();
			var doctorList = _mapper.Map<IEnumerable<Doctor>, IEnumerable<DoctorViewModel>>(doctors);
			return Ok(doctorList);
		}
	}
}