﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Scheduling.DB;
using Scheduling.DB.Entities;
using Scheduling.Models;
using Scheduling.Repository;
using Scheduling.Repository.DataModels;
using Scheduling.Repository.Interfaces;
using System.Collections.Generic;

namespace Scheduling.Controllers
{
	[Route("api/treatment")]
	public class TreatmentController : Controller
	{

		private readonly ITreatmentRepository _treatmentRepository;
		private readonly IDataRepository<Study> _dataRepository;
		private readonly IMapper _mapper;
		private readonly ScheduleContext _dataContext;

		public TreatmentController(IMapper mapper, IDataRepository<Study> dataRepository, ITreatmentRepository treatmentRepository, ScheduleContext context)
		{
			_dataRepository = dataRepository;
			_mapper = mapper;
			_dataContext = context;
			_treatmentRepository = treatmentRepository;
		}

		// GET: api/treatment/
		[HttpGet]
		public IActionResult Get()
		{
			var result = _treatmentRepository.GetStudyList();
			if(result == null)
			{
				return NotFound("Schedule not found.");
			}

			return Ok(_mapper.Map<IEnumerable<StudyDataModel>,IEnumerable<StudyViewModel>>(result));
		}

		// GET: api/treatment/1
		[HttpGet("{id}")]
		public IActionResult Get(long id)
		{
			var result = _treatmentRepository.GetStudyDetails(id);

			if (result == null)
			{
				return NotFound("Schedule not found.");
			}

			return Ok(_mapper.Map<StudyViewModel>(result));
		}

		// POST: api/treatment
		[HttpPost]
		public IActionResult Post([FromBody] StudyViewModel study)
		{
			if (study == null)
			{
				return BadRequest("Data is null.");
			}

			var newStudy = new Study
			{
				Description = study.Description,
				Status = study.Status,
				StartTime = study.StartTime,
				EndTime = study.EndTime,
				PatientId = study.PatientId,
				DoctorId = study.DoctorId,
				RoomId = study.RoomId
			};
			_dataRepository.Add(newStudy);
			var result = _treatmentRepository.GetStudyDetails(newStudy.StudyId);
			
			return Ok(_mapper.Map<StudyViewModel>(result)); ;
		}

		// PUT: api/treatment/1
		[HttpPut("{id}")]
		public IActionResult Put(long id, [FromBody] StudyViewModel study)
		{
			if (study == null)
			{
				return BadRequest("Data is null.");
			}

			var studyDetails = new Study
			{
				Description = study.Description,
				Status = study.Status,
				StartTime = study.StartTime,
				EndTime = study.EndTime,
				PatientId = study.PatientId,
				DoctorId = study.DoctorId,
				RoomId = study.RoomId
			};

			Study studyToUpdate = _dataRepository.Get(id);
			if (studyToUpdate == null)
			{
				return NotFound("Schedule not found.");
			}

			_dataRepository.Update(studyToUpdate, studyDetails);
			return Ok();
		}	
		

	}
}