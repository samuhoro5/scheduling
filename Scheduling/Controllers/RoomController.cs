﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Scheduling.DB.Entities;
using Scheduling.Models;
using Scheduling.Repository;
using System.Collections.Generic;

namespace Scheduling.Controllers
{
	[Route("api/room")]
	public class RoomController : Controller
	{
		private readonly IDataRepository<Room> _dataRepository;
		private readonly IMapper _mapper;

		public RoomController(IMapper mapper, IDataRepository<Room> dataRepository)
		{
			_dataRepository = dataRepository;
			_mapper = mapper;
		}

		// GET: api/Room/1
		[HttpGet]
		public IActionResult Get()
		{
			IEnumerable<Room> rooms = _dataRepository.GetAll();
			var roomList = _mapper.Map<IEnumerable<Room>, IEnumerable<RoomViewModel>>(rooms);
			return Ok(roomList);
		}
	}
}