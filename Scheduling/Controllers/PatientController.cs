﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Scheduling.DB;
using Scheduling.DB.Entities;
using Scheduling.Models;
using Scheduling.Repository;
using Scheduling.Repository.DataModels;
using System.Collections.Generic;

namespace Scheduling.Controllers
{
	[Route("api/patient")]
	public class PatientController : Controller
	{
		private readonly IDataRepository<Patient> _dataRepository;
		private readonly IMapper _mapper;
		private readonly ScheduleContext _dataContext;
		private readonly IPatientRepository _patientRepository;

		public PatientController(IMapper mapper, IDataRepository<Patient> dataRepository, IPatientRepository patientRepository)
		{
			_dataRepository = dataRepository;
			_mapper = mapper;
			_patientRepository = patientRepository;
		}
		

		// GET: api/Patient/1
		[HttpGet("{id}")]
		public IActionResult Get(long id)
		{
			var result = _patientRepository.GetPatientDetails(id);

			if (result == null)
			{
				return NotFound("Patient not found.");
			}
			
			return Ok(_mapper.Map<PatientViewModel>(result));
		}

		// POST: api/Patient
		[HttpPost]
		public IActionResult Post([FromBody] PatientViewModel patient)
		{
			if (patient == null)
			{
				return BadRequest("Patient is null.");
			}
			var newPatient = new Patient
			{
				FirstName = patient.FirstName,
				LastName = patient.LastName,
				Gender = patient.Gender,
				DateOfBirth = patient.DateOfBirth
			};
			_dataRepository.Add(newPatient);
			var result = _patientRepository.GetPatientDetails(newPatient.PatientId);
			return Ok(_mapper.Map<PatientViewModel>(result));
		}

		// GET: api/Patient/UnscheduledPatients
		[HttpGet]
		[Route("UnscheduledPatients")]
		public IActionResult GetUnscheduledPatients()
		{
			var result = _patientRepository.GetUnscheduledPatientList();
			if (result == null)
			{
				return BadRequest("Patient is null.");
			}

			return Ok(_mapper.Map<IEnumerable<PatientDataModel>,IEnumerable<PatientViewModel>>(result));
		}


		// GET: api/Patient/
		[HttpGet]
		public IActionResult Get()
		{
			var result = _patientRepository.GetUnscheduledPatientList();
			if (result == null)
			{
				return BadRequest("Patient is null.");
			}

			return Ok(_mapper.Map<IEnumerable<PatientDataModel>, IEnumerable<PatientViewModel>>(result));
		}
	}
}