﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Scheduling.Models
{
	public class DoctorViewModel
	{
		public long DoctorId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
	}
}
