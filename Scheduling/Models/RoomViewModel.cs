﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Scheduling.Models
{
    public class RoomViewModel
	{
		public long RoomId { get; set; }

		[Required]
		public string Name { get; set; }
	}
}
