﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Scheduling.Models
{
	public class StudyViewModel
    {
		public long StudyId { get; set; }		
		public string Description { get; set; }
		public string Status { get; set; }
		public DateTime StartTime { get; set; }
		public DateTime? EndTime { get; set; }
		public long PatientId { get; set; }
		public long DoctorId { get; set; }
		public long RoomId { get; set; }
		public string PatientName { get; set; }
		public string RoomName { get; set; }
		public string DoctorName { get; set; }
		
	}
}
