Front-end reactjs - 16.12.0 (Scheduling\ClientApp)
Backend -   ASP.NET core 2.1(Scheduling, Scheduling\Scheduling.DB, Scheduling.Repository)
Database - Sql Server

1. To setup frontend and backend on Microsoft visual studio code
Backend designed using code first approach.
>> install all package

To create database use commands in VS Package Manager Console(PMC)
>> Update connection string as required in file "appsettings.json".
>> Open the PMC and select default project "Scheduling.DB"

>> Run the following commands to create database and to insert dummy data into "doctors" and "rooms" tables.
PM >> Add-Migration Scheduling.DB.ScheduleContext
PM >> update-database


Now, Simply build and run the application both frontend and backend project start.

2. if you want to run only Front-end Project ("Scheduling\ClientApp")
>> go to the project directory on command prompt(cmd) and run the below npm command
>> npm i
>> npm start




 
