﻿using Microsoft.EntityFrameworkCore;
using Scheduling.DB.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduling.DB
{
	public class ScheduleContext : DbContext
	{
		public ScheduleContext(DbContextOptions options)
			: base(options)
		{
		}
		public DbSet<Patient> Patients { get; set; }
		public DbSet<Doctor> Doctors { get; set; }
		public DbSet<Room> Rooms { get; set; }
		public DbSet<Study> Studies { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Doctor>().HasData(new Doctor
			{
				DoctorId = 1,
				FirstName = "Rani",
				LastName = "Kumari"
			}, new Doctor
			{
				DoctorId = 2,
				FirstName = "Syam",
				LastName = "Kumar"
			}, new Doctor
			{
				DoctorId = 3,
				FirstName = "Vijay",
				LastName = "Kumar"
			}, new Doctor
			{
				DoctorId = 4,
				FirstName = "Ritik",
				LastName = "Mody"
			}, new Doctor
			{
				DoctorId = 5,
				FirstName = "Naresh",
				LastName = "Sen"
			});

			modelBuilder.Entity<Room>().HasData(new Room
			{
				RoomId = 1,
				Name = "B001"
			}, new Room
			{
				RoomId = 2,
				Name = "B002"
			}, new Room
			{
				RoomId = 3,
				Name = "B003"
			}, new Room
			{
				RoomId = 4,
				Name = "B004"
			}, new Room
			{
				RoomId = 5,
				Name = "B005"
			});
		}
	}

}


