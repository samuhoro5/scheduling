﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Scheduling.DB.Entities
{
	[Table("Studies")]
	public class Study
	{
		[Key]
		public long StudyId { get; set; }
		public long PatientId { get; set; }
		public long DoctorId { get; set; }
		public long RoomId { get; set; }
		[Required]
		public string Description { get; set; }
		[Required]
		public string Status { get; set; }
		public DateTime StartTime { get; set; }
		public DateTime? EndTime { get; set; }
		[Required]

		[ForeignKey("RoomId")]
		public Room Room { get; set; }

		[ForeignKey("PatientId")]
		public virtual Patient Patient { get; set; }

		[ForeignKey("DoctorId")]
		public virtual Doctor Doctor { get; set; }

	}
}
