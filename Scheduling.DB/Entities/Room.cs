﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Scheduling.DB.Entities
{
	[Table("Rooms")]
	public class Room
	{
		[Key]
		public long RoomId { get; set; }
		public string Name { get; set; }
	}
}
