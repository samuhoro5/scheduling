﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Scheduling.DB.Entities
{
	[Table("Doctors")]
	public class Doctor
	{
		[Key]
		public long DoctorId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }

	}
}
